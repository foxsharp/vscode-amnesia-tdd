# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1]

### Fixed

- Fixed the `PropHealth` function signatures not completing all parameters

## [1.1.0]

### Added

- Simple callback intent check
    
    _Should a callback like `AddTimer` reference a simple string signature such as: `AddTimer("", 1.0f, "TimerFunc")`, the `"TimerFunc"` will attempt be attempted to be found in the current file and are diagnosed as errors. Empty strings, or complex parameters such as concatenated strings are ignored._

- Missing string intent recognition

    _After Amnesia 1.5, intents are no longer optional and so the extension reports missing intent on string parameters:_
    ```diff
    - void FuncWithMissingIntent(string foo) { }
    + void FuncWithIntents(string &in foo) { }
    + void FuncWithIntentsOut(string &out foo) { }
    ```

## [1.0.0]

- Initial release