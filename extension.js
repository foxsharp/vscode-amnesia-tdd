const vscode = require('vscode');
const path = require('path');
const fs = require('fs');

/**
 * @param {vscode.ExtensionContext} context
 */
async function activate(context) {

	const jsonPath = path.join(context.extensionPath, 'amnesia-script-functions.json');
	const rawData = fs.readFileSync(jsonPath);
	const data = JSON.parse(rawData);

	const completionItems = data.functions.map(func => {
		const completionItem = new vscode.CompletionItem(
			func.name,
			vscode.CompletionItemKind.Function
		);
		completionItem.documentation = new vscode.MarkdownString(func.documentation);
		completionItem.insertText = new vscode.SnippetString(func.snippet);
		return completionItem;
	});

	const completionItemProvider = vscode.languages.registerCompletionItemProvider('cpp', {
		provideCompletionItems(document, position, token, context) {
			return completionItems;
		}
	}, '.'); // Trigger completion on '.'

	context.subscriptions.push(completionItemProvider);

	const hoverProvider = vscode.languages.registerHoverProvider('cpp', {
		provideHover(document, position, token) {
			const wordRange = document.getWordRangeAtPosition(position, /\b\w+\b/);
			if (!wordRange) {
				return null;
			}

			const word = document.getText(wordRange);
			const func = data.functions.find(f => f.name === word);

			if (func) {
				return new vscode.Hover(new vscode.MarkdownString(func.documentation));
			}
			return null;
		}
	});

	context.subscriptions.push(hoverProvider);

	const callbackConfigPath = path.join(context.extensionPath, 'amnesia-callbacks.json');
	let callbackConfig = [];

	try {
		const configFile = fs.readFileSync(callbackConfigPath, 'utf8');
		callbackConfig = JSON.parse(configFile);
	} catch (error) {
		vscode.window.showErrorMessage('Error loading configuration file: ' + error.message);
		return;
	}

	const diagnosticCollection = vscode.languages.createDiagnosticCollection('amnesiaCallbackValidator');
	context.subscriptions.push(diagnosticCollection);

	const checkDocumentForIssues = (document) => {
		if (document.languageId !== 'cpp') {
			return;
		}

		const diagnostics = [];
		const text = document.getText();

		callbackConfig.forEach(({ FunctionCallRegex, FunctionCallbackRegex }) => {
			const callRegex = new RegExp(FunctionCallRegex, 'g');
			let functionMatch;

			while ((functionMatch = callRegex.exec(text)) !== null) {
				const callbackName = functionMatch[1];
				const callbackRegexStr = FunctionCallbackRegex.replace('{CALLBACK_NAME}', callbackName);
				const callbackRegex = new RegExp(callbackRegexStr, 'g');

				if (!callbackRegex.test(text)) {
					const range = new vscode.Range(
						document.positionAt(functionMatch.index),
						document.positionAt(functionMatch.index + functionMatch[0].length)
					);
					const diagnostic = new vscode.Diagnostic(range, `The callback ${callbackName} could not be found.\nAre you missing intents (&in, &out)?`, vscode.DiagnosticSeverity.Error);
					diagnostics.push(diagnostic);
				}
			}
		});

        const functionSignatureRegex = /([a-zA-Z_]\w*)\s+\w+\s*\(([^)]*)\)/g;
        let signatureMatch;
        while ((signatureMatch = functionSignatureRegex.exec(text)) !== null) {
            const params = signatureMatch[2];
            const paramRegex = /string\s+(\w+)/g;
            let paramMatch;
            let hasMissingIntent = false;
            while ((paramMatch = paramRegex.exec(params)) !== null) {
                const paramName = paramMatch[1];
                const paramWithoutIntentRegex = new RegExp(`string\\s*\\b${paramName}\\b\\s*(?!\\s*\\&\\s*(in|out))`, 'g');
                if (paramWithoutIntentRegex.test(params)) {
                    hasMissingIntent = true;
                }
            }

            if (hasMissingIntent) {
                const start = signatureMatch.index;
                const end = start + signatureMatch[0].length;
                const range = new vscode.Range(
                    document.positionAt(start),
                    document.positionAt(end)
                );
                const diagnostic = new vscode.Diagnostic(
                    range,
                    `Function signature contains string parameters missing intent (e.g., &in or &out)`,
                    vscode.DiagnosticSeverity.Warning
                );
                diagnostics.push(diagnostic);
            }
        }

		diagnosticCollection.set(document.uri, diagnostics);
	};

	context.subscriptions.push(
		vscode.workspace.onDidOpenTextDocument(checkDocumentForIssues),
		vscode.workspace.onDidChangeTextDocument(e => checkDocumentForIssues(e.document)),
		vscode.workspace.onDidSaveTextDocument(checkDocumentForIssues)
	);

	vscode.workspace.textDocuments.forEach(checkDocumentForIssues);
}

function deactivate() { }

module.exports = {
	activate,
	deactivate
}
