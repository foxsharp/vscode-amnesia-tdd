# Amnesia: The Dark Descent Scripting

This extension provides auto-completions and documentation for script functions of Amnesia: The Dark Descent up to version 1.5

## Features

- Hover over a function call for documentation

![screenshot showcasing documentation hover](images/screen1.png)

- Start typing for a list of suggestions, press <kbd>Enter</kbd> to confirm

![screenshot showcasing documentation hover](images/screen2.png)

- After confirming a completion use <kbd>TAB</kbd> and <kbd>Shift + TAB</kbd> to jump between parameters

![screenshot showcasing documentation hover](images/screen3.png)

- Simple Amnesia calls with Callbacks get validation in terms of name and parameter intent

> ⚠️ Since Amnesia 1.5 intents (&in, &out) are not optional

![screenshot](images/screen4.png)

- Any string parameter without an intent will raise a warning

![screenshot](images/screen5.png)

- Complex parameters (such as variable or concatenated params) are not checked

![screenshot](images/screen6.png)


## Known Issues

This extension **IS NOT** compatible with the `C/C++` extension or any other extension providing support for C++ language. This may be fixed in the future.
